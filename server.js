import express from 'express';
import path from 'path';
import exphbs from 'express-handlebars';

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + '/views'));
app.set('views', path.join(__dirname, '/views'));
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/signin', (req, res) => {
  res.render('signin');
});

app.get('/signup', (req, res) => {
  res.render('signup');
});

app.get('/search', (req, res) => {
  res.render('search');
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}...`)
});